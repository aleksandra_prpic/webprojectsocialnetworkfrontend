///<reference path="../../theme/validators/link.validator.ts"/>
import {Component, ElementRef, OnInit} from '@angular/core';
import {AuthService} from '../../theme/services/auth-service/auth.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Project} from '../../theme/services/content-service/project.model';
import {ContentService} from '../../theme/services/content-service/content.service';
import {LinkValidator} from '../../theme/validators/link.validator';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'edit-project',
  templateUrl: './edit-project.component.html',
})
export class EditProjectComponent implements OnInit {
  host = 'http://localhost:3002/';
  isNewImageSet: boolean;
  projectId: string;
  project: Project = new Project('', '', '');
  private sub;
  public form: FormGroup;
  public title: AbstractControl;
  public content: AbstractControl;
  public link: AbstractControl;
  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router,
  private contentService: ContentService, private el: ElementRef, private route: ActivatedRoute, private toastr: ToastrService) {
    this.form = fb.group({
      'title': ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(100)])],
      'content': ['', Validators.compose([Validators.required])],
      'link': ['', Validators.compose([LinkValidator.validate])]
    });
    this.title = this.form.controls['title'];
    this.content = this.form.controls['content'];
    this.link = this.form.controls['link'];
  }
  public onSubmit(values: any): void {
    if (!this.isNewImageSet) {
      this.contentService.modifyProject(this.project)
        .subscribe(response => {
          this.toastr.success('Changes saved successfully.', 'Success!');
          this.router.navigateByUrl('/');
        }, error => this.toastr.warning(error.message, 'Error'));
    } else {
      let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#projectImg');
      Promise.all([
        this.contentService.modifyProject(this.project).toPromise(),
        this.contentService.uploadFile(inputEl, `${this.host}projects/${this.projectId}`, 'imageUrl').toPromise()
      ]).then(responses => {
        this.router.navigateByUrl('/');
        this.toastr.success('Changes saved successfully.', 'Success!');
      }).catch(error => this.toastr.warning(error.message, 'Error'));
    }
  };
  isLoggedIn() { return this.authService.isLoggedIn(); }
  onImageSelect(e) { return this.isNewImageSet = e.target.files.length > 0; }
  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.projectId = params['id'];
      this.contentService.getProjectById(this.projectId)
        .subscribe(x => this.project = x, err => this.toastr.warning(err.message, 'Error'));
    });
  }
}

