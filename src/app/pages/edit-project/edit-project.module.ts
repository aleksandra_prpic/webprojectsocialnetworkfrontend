import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {EditProjectComponent} from './edit-project.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import '/node_modules/froala-editor/js/froala_editor.pkgd.min.js';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

@NgModule({
  imports: [ FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(), BrowserModule, RouterModule, ReactiveFormsModule, FormsModule  ],
  declarations: [ EditProjectComponent ],
  exports: [ EditProjectComponent ]
})
export class EditProjectModule {}
