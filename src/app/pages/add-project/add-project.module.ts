import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {AddProjectComponent} from './add-project.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import '/node_modules/froala-editor/js/froala_editor.pkgd.min.js';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

@NgModule({
  imports: [ FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(), BrowserModule, RouterModule, ReactiveFormsModule, FormsModule  ],
  declarations: [ AddProjectComponent ],
  exports: [ AddProjectComponent ]
})
export class AddProjectModule {}
