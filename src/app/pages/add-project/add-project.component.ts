///<reference path="../../theme/validators/link.validator.ts"/>
import {Component, ElementRef} from '@angular/core';
import {AuthService} from '../../theme/services/auth-service/auth.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Project} from '../../theme/services/content-service/project.model';
import {ContentService} from '../../theme/services/content-service/content.service';
import {LinkValidator} from '../../theme/validators/link.validator';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'add-project',
  templateUrl: './add-project.component.html',
})
export class AddProjectComponent{
  host = 'http://localhost:3002/';
  isNewImageSet: boolean;
  public form: FormGroup;
  public title: AbstractControl;
  public content: AbstractControl;
  public link: AbstractControl;
  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router,
  private contentService: ContentService, private el: ElementRef, private toastr: ToastrService) {
    this.form = fb.group({
      'title': ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(100)])],
      'content': ['', Validators.compose([Validators.required])],
      'link': ['', Validators.compose([LinkValidator.validate])]
    });
    this.title = this.form.controls['title'];
    this.content = this.form.controls['content'];
    this.link = this.form.controls['link'];
  }
  public onSubmit(values: any): void {
    if (!this.isNewImageSet) {
      const project = new Project(values.title, values.content, values.link);
      this.contentService.addProject(project)
        .subscribe(response => {
          this.toastr.success('Project has been added.', 'Success!');
          this.router.navigateByUrl('/');
        }, error => this.toastr.warning(error.message, 'Error'));
    } else {
      let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#projectImg');
      const project = new Project(values.title, values.content, values.link);
        this.contentService.addProject(project).toPromise()
        .then(responseProject => {
          this.contentService.uploadFile(inputEl, `${this.host}projects/${responseProject._id}`, 'imageUrl').toPromise()
            .then(response => {
              this.toastr.success('Project has been added.', 'Success!');
              this.router.navigateByUrl('/'); }, err => this.toastr.warning(error.message, 'Error'));
      }).catch(err => this.toastr.warning(error.message, 'Error'));
    }

  };
  isLoggedIn() { return this.authService.isLoggedIn(); }
  onImageSelect(e) { return this.isNewImageSet = e.target.files.length > 0; }
}

