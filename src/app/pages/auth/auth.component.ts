import { Component } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../theme/services/auth-service/auth.service';
import {User} from '../../theme/services/auth-service/user.model';
import {Router} from '@angular/router';
import {EqualPasswordsValidator, EmailValidator} from '../../theme/validators/index';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent  {
  public form: FormGroup;
  public username: AbstractControl;
  public password: AbstractControl;

  public formRegister: FormGroup;
  public firstNameReg: AbstractControl;
  public lastNameReg: AbstractControl;
  public usernameReg: AbstractControl;
  public emailReg: AbstractControl;
  public passwords: FormGroup;
  public passwordReg: AbstractControl;
  public password2Reg: AbstractControl;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private toastr: ToastrService) {
    this.form = fb.group({
      'username': ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(15)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])]
    });
    this.username = this.form.controls['username'];
    this.password = this.form.controls['password'];

    this.formRegister = fb.group({
      'usernameReg': ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(15)])],
      'firstNameReg': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(15)])],
      'lastNameReg': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(15)])],
      'emailReg': ['', Validators.compose([Validators.required, EmailValidator.validate])],
      'passwords': fb.group({
        'passwordReg': ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])],
        'password2Reg': ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])]
      }, {validator: EqualPasswordsValidator.validate('passwordReg', 'password2Reg')})
    });
    this.usernameReg = this.formRegister.controls['usernameReg'];
    this.firstNameReg = this.formRegister.controls['firstNameReg'];
    this.lastNameReg = this.formRegister.controls['lastNameReg'];
    this.emailReg = this.formRegister.controls['emailReg'];
    this.passwords = <FormGroup> this.formRegister.controls['passwords'];
    this.passwordReg = this.passwords.controls['passwordReg'];
    this.password2Reg = this.passwords.controls['password2Reg'];
  }

  public onSubmitLogin(values: any, redirectToSettings = false): void {
    const user = new User(values.username, values.password);
    this.authService.login(user)
      .subscribe(response => {
        localStorage.setItem('token', response.token);
        localStorage.setItem('userId', response.userId);
        localStorage.setItem('username', response.username);
        localStorage.setItem('userAvatar', response.userAvatar);
        localStorage.setItem('userFirstName', response.userFirstName);
        localStorage.setItem('userLastName', response.userLastName);
        if (redirectToSettings) {
          this.toastr.info('If you want to, fill in additional information about yourself here.', 'Welcome!');
          this.router.navigateByUrl('/editProfile');
        } else {
          this.toastr.success(`Welcome back, ${localStorage.getItem('username')}.`, 'Logged in!');
          this.router.navigateByUrl('/');
        }
      }, error => this.toastr.warning(error.message, 'Error'));
  }
  public onSubmitRegistration(values: any): void {
    const user = new User(values.usernameReg, values.passwords.passwordReg, values.firstNameReg, values.lastNameReg, values.emailReg);
    this.authService.register(user)
      .subscribe(response => {
        this.onSubmitLogin({username: values.usernameReg, password: values.passwords.passwordReg}, true);
      }, error => this.toastr.warning(error.message, 'Error'));
  }
}
