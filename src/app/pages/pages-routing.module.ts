import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {AuthComponent} from './auth/auth.component';
import {SingleProjectPageComponent} from './single-project-page/single-project-page.component';
import {ProfileComponent} from './profile/profile.component';
import {EditProfileComponent} from './edit-profile/edit-profile.component';
import {AddProjectComponent} from './add-project/add-project.component';
import {EditProjectComponent} from './edit-project/edit-project.component';
import {AuthGuard} from '../theme/services/auth-guard/auth.guard';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  {path: 'auth', component: AuthComponent },
  {path: 'project/:id', component: SingleProjectPageComponent},
  {path: 'profile/:id', component: ProfileComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'editProfile', canActivate: [AuthGuard], component: EditProfileComponent},
  {path: 'addProject', canActivate: [AuthGuard], component: AddProjectComponent},
  {path: 'editProject/:id', canActivate: [AuthGuard], component: EditProjectComponent},
  {path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
