import { Component } from '@angular/core';
import {AuthService} from '../../../../theme/services/auth-service/auth.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EqualPasswordsValidator} from '../../../../theme/validators/equalPasswords.validator';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent  {
  public form: FormGroup;
  public currentPw: AbstractControl;
  public passwords: FormGroup;
  public newPw: AbstractControl;
  public newPw2: AbstractControl;

  constructor(private authService: AuthService, private fb: FormBuilder, private toastr: ToastrService) {
    this.form = fb.group({
      'currentPw': ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])],
      'passwords': fb.group({
        'newPw': ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])],
        'newPw2': ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])]
      }, {validator: EqualPasswordsValidator.validate('newPw', 'newPw2')})
    });
    this.currentPw = this.form.controls['currentPw'];
    this.passwords = <FormGroup> this.form.controls['passwords'];
    this.newPw = this.passwords.controls['newPw'];
    this.newPw2 = this.passwords.controls['newPw2'];
  }

  isLoggedIn() { return this.authService.isLoggedIn(); }
  onSubmit(values) {
    this.authService.changePassword({current: values.currentPw, new: values.passwords.newPw})
      .subscribe(data => this.toastr.success('Password has been changed.', 'Done'),
          err => this.toastr.warning(err.message, 'Error'));
  }
}

