import {Component, ElementRef, OnInit } from '@angular/core';
import {AuthService} from '../../../../theme/services/auth-service/auth.service';
import {UsersService} from '../../../../theme/services/users-service/users.service';
import {User} from '../../../../theme/services/auth-service/user.model';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EmailValidator, GenderValidator, MobileNumberValidator } from '../../../../theme/validators/index';
import {ContentService} from '../../../../theme/services/content-service/content.service';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'profile-information',
  templateUrl: './profile-information.component.html',
})
export class ProfileInformationComponent implements OnInit {
  host = 'http://localhost:3002/';
  user: User = new User('', '');
  originalUser: User = new User('', '');
  isNewAvatarSet: boolean;
  public form: FormGroup;
  public username: AbstractControl;
  public firstName: AbstractControl;
  public lastName: AbstractControl;
  public gender: AbstractControl;
  public job: AbstractControl;
  public email: AbstractControl;
  public mobileNumber: AbstractControl;
  public aboutMe: AbstractControl;

  constructor(private authService: AuthService, private userService: UsersService,
              private fb: FormBuilder, private el: ElementRef, private contentService: ContentService, private toastr: ToastrService) {
    this.form = fb.group({
      'username': ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(15)])],
      'firstName': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(15)])],
      'lastName': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(15)])],
      'gender': ['', Validators.compose([GenderValidator.validate])],
      'job': ['', Validators.compose([])],
      'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
      'mobileNumber': ['', Validators.compose([ MobileNumberValidator.validate ])],
      'aboutMe': ['', Validators.compose([])]
    });
    this.username = this.form.controls['username'];
    this.firstName = this.form.controls['firstName'];
    this.lastName = this.form.controls['lastName'];
    this.gender = this.form.controls['gender'];
    this.job = this.form.controls['job'];
    this.email = this.form.controls['email'];
    this.mobileNumber = this.form.controls['mobileNumber'];
    this.aboutMe = this.form.controls['aboutMe'];
  }
  isLoggedIn() { return this.authService.isLoggedIn(); }
  ngOnInit(): void {
    this.userService.getUserById(localStorage.getItem('userId'))
      .subscribe(user => { this.user = user; return this.originalUser = user; });
  }
  onSubmit(values): void {
    if ( this.isNewAvatarSet ) {
      let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#avatar');
      Promise.all([
        this.userService.modifyUser(values as User, localStorage.getItem('userId')).toPromise(),
        this.contentService.uploadFile(inputEl, `${this.host}users/${localStorage.getItem('userId')}`, 'avatar').toPromise()
      ]).then(responses => {
        if (values.username !== localStorage.getItem('username')) {
          localStorage.setItem('username', values.username);
        }
        if (values.firstName !== localStorage.getItem('userFirstName')) {
          localStorage.setItem('userFirstName', values.firstName);
        }
        if (values.lastName !== localStorage.getItem('userLastName')) {
          localStorage.setItem('userLastName', values.lastName);
        }
        localStorage.setItem('userAvatar', responses[1].avatar);
        this.toastr.success('Changes saved successfully.', 'Success!');
      }).catch(err => this.toastr.warning(err.message, 'Error'));
    } else {
      this.userService.modifyUser(values as User, localStorage.getItem('userId'))
        .subscribe(data => {
          if (values.username !== localStorage.getItem('username')) {
            localStorage.setItem('username', values.username);
          }
          if (values.firstName !== localStorage.getItem('userFirstName')) {
            localStorage.setItem('userFirstName', values.firstName);
          }
          if (values.lastName !== localStorage.getItem('userLastName')) {
            localStorage.setItem('userLastName', values.lastName);
          }
          this.toastr.success('Changes saved successfully.', 'Success!');
        }, err => this.toastr.warning(err.message, 'Error'));
    }
  }
  onAvatarSelect(e) { return this.isNewAvatarSet = e.target.files.length > 0; }
}

