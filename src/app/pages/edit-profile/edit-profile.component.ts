import { Component } from '@angular/core';
import {AuthService} from '../../theme/services/auth-service/auth.service';


@Component({
  selector: 'edit-profile',
  templateUrl: './edit-profile.component.html',
})
export class EditProfileComponent  {
  constructor(private authService: AuthService) {}

  isLoggedIn() { return this.authService.isLoggedIn(); }
}

