import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {EditProfileComponent} from './edit-profile.component';
import {ProfileInformationComponent} from './components/profile-information/profile-information.component';
import {RouterModule} from '@angular/router';
import {ChangePasswordComponent} from './components/change-password/change-password.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [ BrowserModule, RouterModule, ReactiveFormsModule, FormsModule  ],
  declarations: [ EditProfileComponent, ProfileInformationComponent, ChangePasswordComponent ],
  exports: [ EditProfileComponent ]
})
export class EditProfileModule {}
