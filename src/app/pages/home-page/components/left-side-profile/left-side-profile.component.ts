import { Component } from '@angular/core';
import {AuthService} from '../../../../theme/services/auth-service/auth.service';

@Component({
  selector: 'left-side-profile',
  templateUrl: './left-side-profile.component.html',
})

export class LeftSideProfileComponent  {
  avatar = `http://localhost:3002/${localStorage.getItem('userAvatar')}`;
  username = localStorage.getItem('username');
  fullName = `${localStorage.getItem('userFirstName')} ${localStorage.getItem('userLastName')}`;
  constructor(private authService: AuthService) {
  }
  isLoggedIn() { return this.authService.isLoggedIn(); }
}

