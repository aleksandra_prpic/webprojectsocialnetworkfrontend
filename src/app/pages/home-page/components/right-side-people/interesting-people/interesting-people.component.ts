import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../../../../theme/services/users-service/users.service';
import {User} from '../../../../../theme/services/auth-service/user.model';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'interesting-people',
  templateUrl: './interesting-people.component.html',
})
export class InterestingPeopleComponent implements OnInit {
  users: User[];
  constructor(private usersService: UsersService, private toastr: ToastrService) {}
  ngOnInit(): void {
    if (localStorage.getItem('userId')) {
      this.usersService.getUsers(3, true)
        .subscribe(response => this.users = response.filter(x => x._id !== localStorage.getItem('userId')),
            error => this.toastr.warning(error.message, 'Error'));
    } else {
      this.usersService.getUsers(3, true)
        .subscribe(response => this.users = response, error => this.toastr.warning(error.message, 'Error'));
    }
  }

}
