import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';
import {ContentService} from '../../../../theme/services/content-service/content.service';
import {Project} from '../../../../theme/services/content-service/project.model';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../../theme/services/auth-service/auth.service';
import {User} from '../../../../theme/services/auth-service/user.model';
import { RouterModule } from '@angular/router';
import {UsersService} from '../../../../theme/services/users-service/users.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentService} from '../../../../theme/services/comment-service/comment.service';
import {Comment} from '../../../../theme/services/comment-service/comment.model';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';

declare let swal: any;

@Component({
  selector: 'centered-projects',
  templateUrl: './centered-projects.component.html',
  styles: [`
    .notLoggedInWrapper > span {
      float: left;
      font-size: 1.6em;
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      margin: 5px;
    }
  `]
})


export class CenteredProjectsComponent implements OnInit {
  avatar = `http://localhost:3002/${localStorage.getItem('userAvatar')}`;
  host = 'http://localhost:3002/';
  projects: Project[];
  users: User[];
  public formComment: FormGroup;
  public comment: AbstractControl;

  constructor(private contentService: ContentService,
              private usersService: UsersService,
              private commentService: CommentService,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private authService: AuthService,
              private toastr: ToastrService) {
    this.formComment = fb.group({
      'comment': ['', Validators.compose([Validators.required])]
    });
    this.comment = this.formComment.controls['comment'];
  }

  ngOnInit(): void {
    this.contentService.getAllProjects(true)
      .subscribe(projects => this.projects = projects, error => this.toastr.warning(error.message, 'Error'));
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }
  isOwner(userId) {
    return localStorage.getItem('userId') && userId === localStorage.getItem('userId');
  }

  public onSubmit(projectId: string, values: any): void {
    this.commentService.postComment(projectId, new Comment(values.comment))
      .subscribe(message => {
        if ($(`.${projectId} > .box-comments:last`).length) {
          $(`.${projectId} > .box-comments:last`).after(`
          <div class="box-footer box-comments" style="display: block;">
              <div class="box-comment">
                <img class="img-circle img-sm" src="${this.host}${localStorage.getItem('userAvatar')}" alt="User Image">
                <div class="comment-text">
                          <span class="username">
                          ${localStorage.getItem('username')}
                          <span class="text-muted pull-right">${moment().fromNow()}</span>
                          </span>
                  ${values.comment}
                </div>
              </div>
            </div>
        `);
          this.contentService.getAllProjects(true)
            .subscribe(projects => this.projects = projects, error => this.toastr.warning(error.message, 'Error'));
        } else {
          $(`.${projectId} > .box-body`).after(`
            <div class="box-footer box-comments" style="display: block;">
                <div class="box-comment">
                  <img class="img-circle img-sm" src="${this.host}${localStorage.getItem('userAvatar')}" alt="User Image">
                  <div class="comment-text">
                            <span class="username">
                            ${localStorage.getItem('username')}
                            <span class="text-muted pull-right">${moment().fromNow()}</span>
                            </span>
                    ${values.comment}
                  </div>
                </div>
              </div>
          `);
          this.contentService.getAllProjects(true)
            .subscribe(projects => this.projects = projects, error => this.toastr.warning(error.message, 'Error'));
        }
        this.formComment.reset();
      }, error => this.toastr.warning(error.message, 'Error'));
    this.formComment.reset();
  }

  public loadMore(count: number): void {
    this.contentService.getAllProjects(true, count + this.projects.length).subscribe(projects => {
     return this.projects = projects;
    }, error => this.toastr.warning('Error while loading more projects. Try again', 'Error'));
  }
  public canDeleteComment(userId: string){
    return this.isLoggedIn() && localStorage.getItem('userId') && localStorage.getItem('userId') === userId;
  }
  public deleteComment(projectId: string, commentId: string, project: any) {
    swal({
      title: 'Are you sure?',
      text: 'You can not undo this operation.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.commentService.deleteComment(projectId, commentId)
        .subscribe((response) => {
          project.remove();
          this.toastr.success('Comment has been deleted successfully.', 'Success!');
        }, error => this.toastr.warning(error.message, 'Error'));
        }, (dismiss) => {
          if (dismiss === 'cancel' || dismiss === 'close') {
            return;
          }
        });
  }
  public deleteProject(projectId: string, project: any) {
    swal({
      title: 'Are you sure?',
      text: 'You can not undo this operation.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.contentService.deleteProjectById(projectId)
        .subscribe((response) => {
          project.remove();
          this.toastr.success('Project has been deleted successfully.', 'Success!');
        } , error => this.toastr.warning(error.message, 'Error'));
    }, (dismiss) => {
      if (dismiss === 'cancel' || dismiss === 'close') {
        return;
      }
    });
  }
}
