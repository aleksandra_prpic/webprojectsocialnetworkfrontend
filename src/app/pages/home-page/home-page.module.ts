import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HomePageComponent} from './home-page.component';
import {LeftSideProfileComponent} from './components/left-side-profile/left-side-profile.component';
import {CenteredProjectsComponent} from './components/centered-projects/centered-projects.component';
import {RightSidePeopleComponent} from './components/right-side-people/right-side-people.component';
import {InterestingPeopleComponent} from './components/right-side-people/interesting-people/interesting-people.component';
import {MomentModule} from 'angular2-moment';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports:      [ BrowserModule, MomentModule, ReactiveFormsModule,
    FormsModule, RouterModule],
  declarations: [ HomePageComponent, LeftSideProfileComponent, CenteredProjectsComponent, RightSidePeopleComponent,
    InterestingPeopleComponent ],
  exports: [ HomePageComponent ]
})
export class HomePageModule {}
