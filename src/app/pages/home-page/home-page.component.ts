import { Component } from '@angular/core';
import {AuthService} from '../../theme/services/auth-service/auth.service';

@Component({
  selector: 'home-page',
  templateUrl: './home-page.component.html',
})
export class HomePageComponent  {
  constructor(private authService: AuthService) {}

  isLoggedIn() { return this.authService.isLoggedIn(); }
}

