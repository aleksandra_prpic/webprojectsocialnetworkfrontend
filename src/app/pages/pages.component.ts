import {Component} from '@angular/core';

@Component({
  selector: 'pages',
  template: `
    <navigation></navigation>
    <router-outlet></router-outlet>
    <main-footer></main-footer>
  `,

})
export class PagesComponent {}
