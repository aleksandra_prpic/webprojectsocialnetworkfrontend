import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {SingleProjectPageComponent} from './single-project-page.component';
import {CenteredProjectComponent} from './components/centered-project/centered-project.component';
import {MomentModule} from 'angular2-moment';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports:      [ BrowserModule, MomentModule, ReactiveFormsModule,
    FormsModule, RouterModule ],
  declarations: [ SingleProjectPageComponent, CenteredProjectComponent ],
  exports: [ SingleProjectPageComponent ]
})
export class SingleProjectPageModule {}

