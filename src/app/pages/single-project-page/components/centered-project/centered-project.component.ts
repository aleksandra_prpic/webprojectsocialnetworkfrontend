import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ContentService} from '../../../../theme/services/content-service/content.service';
import {Project} from '../../../../theme/services/content-service/project.model';
import {CommentService} from '../../../../theme/services/comment-service/comment.service';
import {Comment} from '../../../../theme/services/comment-service/comment.model';
import {AuthService} from '../../../../theme/services/auth-service/auth.service';
import * as moment from 'moment';
import * as $ from 'jquery';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../../theme/services/auth-service/user.model';

declare let swal: any;

@Component({
  selector: 'centered-project',
  templateUrl: './centered-project.component.html',
})
export class CenteredProjectComponent implements OnInit, OnDestroy  {
  host = 'http://localhost:3002/';
  userAvatar = localStorage.getItem('userAvatar');
  projectId: string;
  project = new Project('', '', '');
  user = new User('', '');
  sub: any;
  public formComment: FormGroup;
  public comment: AbstractControl;
  constructor(private route: ActivatedRoute, private fb: FormBuilder , private contentService: ContentService,
              private commentService: CommentService, private authService: AuthService,
              private router: Router, private toastr: ToastrService ) {
    this.formComment = fb.group({
      'comment': ['', Validators.compose([Validators.required])]
    });
    this.comment = this.formComment.controls['comment'];
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
     this.projectId = params['id'];
     this.contentService.getProjectById(this.projectId)
       .subscribe(project => { this.project = project; this.user = project.user; }, error => this.toastr.warning(error.message, 'Error'));
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  isLoggedIn() {
    return this.authService.isLoggedIn();
  }
  isOwner(userId) {
    return localStorage.getItem('userId') && userId === localStorage.getItem('userId');
  }
  public onSubmit(projectId: string, values: any): void {
    this.commentService.postComment(projectId, new Comment(values.comment))
      .subscribe(message => {
        if ($('.box-comments:last').length) {
          $(`.${projectId} > .box-comments:last`).after(`
          <div class="box-footer box-comments" style="display: block;">
              <div class="box-comment">
                <img class="img-circle img-sm" src="${this.host}${localStorage.getItem('userAvatar')}" alt="User Image">
                <div class="comment-text">
                          <span class="username">
                          ${localStorage.getItem('username')}
                          <span class="text-muted pull-right">${moment().fromNow()}</span>
                          </span>
                  ${values.comment}
                </div>
              </div>
            </div>
        `);
          this.contentService.getProjectById(this.projectId)
            .subscribe(project => this.project = project, error => this.toastr.warning(error.message, 'Error'));
        } else {
          $('.box-body').after(`
            <div class="box-footer box-comments" style="display: block;">
                <div class="box-comment">
                  <img class="img-circle img-sm" src="${this.host}${localStorage.getItem('userAvatar')}" alt="User Image">
                  <div class="comment-text">
                            <span class="username">
                            ${localStorage.getItem('username')}
                            <span class="text-muted pull-right">${moment().fromNow()}</span>
                            </span>
                    ${values.comment}
                  </div>
                </div>
              </div>
          `);
          this.contentService.getProjectById(this.projectId)
            .subscribe(project => this.project = project, error => this.toastr.warning(error.message, 'Error'));
        }
        this.formComment.reset();
      }, error => this.toastr.warning(error.message, 'Error'));
    this.formComment.reset();
  }
  public canDeleteComment(userId: string){
    return this.isLoggedIn() && localStorage.getItem('userId') && localStorage.getItem('userId') === userId;
  }
  public deleteComment(projectId: string, commentId: string, project: any) {
    swal({
      title: 'Are you sure?',
      text: 'You can not undo this operation.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.commentService.deleteComment(projectId, commentId)
        .subscribe((response) => {
          project.remove();
          this.toastr.success('Comment has been deleted successfully.', 'Success!');
        }, error => this.toastr.warning(error.message, 'Error'));
    }, (dismiss) => {
      if (dismiss === 'cancel' || dismiss === 'close') {
        return;
      }
    });
  }
  public deleteProject(projectId: string, project: any) {
    swal({
      title: 'Are you sure?',
      text: 'You can not undo this operation.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.contentService.deleteProjectById(projectId)
        .subscribe((response) => {
          project.remove();
          this.toastr.success('Project has been deleted successfully.', 'Success!');
        } , error => this.toastr.warning(error.message, 'Error'));
    }, (dismiss) => {
      if (dismiss === 'cancel' || dismiss === 'close') {
        return;
      }
    });
  }
}

