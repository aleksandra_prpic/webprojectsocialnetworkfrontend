import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {PagesRoutingModule} from './pages-routing.module';
import {AuthModule} from './auth/auth.module';
import {NavigationModule} from '../theme/components/navigation/navigation.module';
import {FooterModule} from '../theme/components/footer/footer.module';
import {HomePageModule} from './home-page/home-page.module';

import {PagesComponent} from './pages.component';
import {SingleProjectPageModule} from './single-project-page/single-project-page.module';
import {ProfileModule} from './profile/profile.module';
import {EditProfileModule} from './edit-profile/edit-profile.module';
import {AddProjectModule} from './add-project/add-project.module';
import {EditProjectModule} from './edit-project/edit-project.module';

@NgModule({
  imports: [ BrowserModule, PagesRoutingModule, AuthModule, NavigationModule, FooterModule,
    HomePageModule, SingleProjectPageModule, ProfileModule, EditProfileModule, AddProjectModule, EditProjectModule ],
  declarations: [ PagesComponent ],
  exports: [ PagesComponent ]
})
export class PagesModule {}
