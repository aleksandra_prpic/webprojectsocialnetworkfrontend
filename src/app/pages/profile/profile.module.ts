import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {ProfileComponent} from './profile.component';
import {CoverComponent} from './components/cover/cover.component';
import {AboutComponent} from './components/about/about.component';
import {TimelineProjectsComponent} from './components/timeline-projects/timeline-projects.component';
import {AboutMeComponent} from './components/about-me/about-me.component';
import {MomentModule} from 'angular2-moment';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProfileService} from './profile.service';
import {RouterModule} from '@angular/router';

@NgModule({
  imports:      [ BrowserModule, MomentModule, ReactiveFormsModule,
    FormsModule, RouterModule ],
  declarations: [ ProfileComponent, CoverComponent, AboutComponent, TimelineProjectsComponent, AboutMeComponent ],
  providers: [ ProfileService ],
  exports: [ ProfileComponent ]
})
export class ProfileModule {}
