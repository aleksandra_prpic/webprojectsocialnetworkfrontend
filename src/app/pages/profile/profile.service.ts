import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {ContentService} from '../../theme/services/content-service/content.service';
import {UsersService} from '../../theme/services/users-service/users.service';

@Injectable()
export class ProfileService {
  private userId: string;
  constructor(private http: Http, private contentService: ContentService, private usersService: UsersService) {}
  init(userId) {
   this.userId = userId;
  }
  getProjects() {
    return this.contentService.getProjectsByUser(this.userId);
  }

  getUser() {
      return this.usersService.getUserById(this.userId);
  }
  getProfileId() {
    return this.userId;
  }
}
