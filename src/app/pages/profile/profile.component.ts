import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProfileService} from './profile.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit, OnDestroy {
  private profileId: string;
  private sub;
  constructor(private profileService: ProfileService, private route: ActivatedRoute) {}
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.profileId = params['id'];
      if (this.profileId) {
        this.profileService.init(this.profileId);
      } else {
        this.profileService.init(localStorage.getItem('userId'));
      }
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
