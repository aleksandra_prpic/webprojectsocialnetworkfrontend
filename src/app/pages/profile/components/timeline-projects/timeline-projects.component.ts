import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../theme/services/auth-service/auth.service';
import {ContentService} from '../../../../theme/services/content-service/content.service';
import {Project} from '../../../../theme/services/content-service/project.model';
import {User} from '../../../../theme/services/auth-service/user.model';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentService} from '../../../../theme/services/comment-service/comment.service';
import {Comment} from '../../../../theme/services/comment-service/comment.model';
import {ProfileService} from '../../profile.service';
import {ToastrService} from 'ngx-toastr';

declare let swal: any;

@Component({
  selector: 'timeline-projects',
  templateUrl: './timeline-projects.component.html',
  styles: [`
    .notLoggedInWrapper > span {
      float: left;
      font-size: 1.6em;
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      margin: 5px;
    }
  `]
})
export class TimelineProjectsComponent implements OnInit {
  host = 'http://localhost:3002/';
  avatar: string;
  public user = new User('usr', 'protected');
  public localUser = localStorage.getItem('userId') || '';
  public userId = '';
  public projects = [new Project('dummy', 'dummy')];
  public formComment: FormGroup;
  public comment: AbstractControl;
  constructor(private authService: AuthService, private fb: FormBuilder, private commentService: CommentService,
              private contentService: ContentService, private profileService: ProfileService, private toastr: ToastrService) {
    this.formComment = fb.group({
      'comment': ['', Validators.compose([Validators.required])]
    });
    this.comment = this.formComment.controls['comment'];
  }
  isLoggedIn() { return this.authService.isLoggedIn(); }
  isOwner(userId) {
    return localStorage.getItem('userId') && userId === localStorage.getItem('userId');
  }
  ngOnInit() {
    this.profileService.getProjects().subscribe(x => this.projects = x.reverse());
    this.profileService.getUser().subscribe(x => {this.userId = x._id; this.user = x as User; this.avatar = x.avatar; });
  }

  public onSubmit(projectId: string, values: any): void {
    this.commentService.postComment(projectId, new Comment(values.comment))
      .subscribe(message => {
          this.profileService.getProjects().subscribe(x => this.projects = x.reverse());
      }, error => this.toastr.warning(error.message, 'Error'));
    this.formComment.reset();
  }
  public canDeleteComment(userId: string) {
    return this.isLoggedIn() && localStorage.getItem('userId') && localStorage.getItem('userId') === userId;
  }
  public deleteComment(projectId: string, commentId: string, project: any) {
    swal({
      title: 'Are you sure?',
      text: 'You can not undo this operation.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.commentService.deleteComment(projectId, commentId)
        .subscribe((response) => {
          project.remove();
          this.toastr.success('Comment has been deleted successfully.', 'Success!');
        }, error => this.toastr.warning(error.message, 'Error'));
    }, (dismiss) => {
      if (dismiss === 'cancel' || dismiss === 'close') {
        return;
      }
    });
  }
  public deleteProject(projectId: string, project: any) {
    swal({
      title: 'Are you sure?',
      text: 'You can not undo this operation.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.contentService.deleteProjectById(projectId)
        .subscribe((response) => {
          project.remove();
          this.toastr.success('Project has been deleted successfully.', 'Success!');
        } , error => this.toastr.warning(error.message, 'Error'));
    }, (dismiss) => {
      if (dismiss === 'cancel' || dismiss === 'close') {
        return;
      }
    });
  }
}

