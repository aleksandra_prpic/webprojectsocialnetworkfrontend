import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../theme/services/auth-service/auth.service';
import {User} from '../../../../theme/services/auth-service/user.model';
import {ProfileService} from '../../profile.service';


@Component({
  selector: 'cover',
  templateUrl: './cover.component.html',
})
export class CoverComponent  implements OnInit {
  host = 'http://localhost:3002/';
  user: User;
  constructor(private authService: AuthService, private profileService: ProfileService) {}
  isLoggedIn() { return this.authService.isLoggedIn(); }
  ngOnInit() {
    this.user = new User('usr', 'protected');
    this.profileService.getUser().subscribe(x => this.user = x );
  }
}

