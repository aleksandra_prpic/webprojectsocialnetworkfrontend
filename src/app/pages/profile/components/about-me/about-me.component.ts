import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../theme/services/auth-service/auth.service';
import {User} from '../../../../theme/services/auth-service/user.model';
import {ProfileService} from '../../profile.service';


@Component({
  selector: 'about-me',
  templateUrl: './about-me.component.html',
})
export class AboutMeComponent implements OnInit{
  host = 'http://localhost:3002/';
  userAbout: string;
  constructor(private authService: AuthService, private profileService: ProfileService) {}
  isLoggedIn() { return this.authService.isLoggedIn(); }
  ngOnInit() {
    this.profileService.getUser().subscribe(x => this.userAbout = x.aboutMe);
  }
}

