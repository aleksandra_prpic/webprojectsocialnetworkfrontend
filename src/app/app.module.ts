import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';

import { AppComponent }  from './app.component';
import {PagesModule} from './pages/pages.module';
import {AuthService} from './theme/services/auth-service/auth.service';
import {ContentService} from './theme/services/content-service/content.service';
import {CommentService} from './theme/services/comment-service/comment.service';
import {UsersService} from './theme/services/users-service/users.service';
import {AuthGuard} from './theme/services/auth-guard/auth.guard';


@NgModule({
  imports:      [ BrowserModule, PagesModule, HttpModule, BrowserAnimationsModule, ToastrModule.forRoot() ],
  declarations: [ AppComponent ],
  providers: [AuthService, ContentService, CommentService, UsersService, AuthGuard],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
