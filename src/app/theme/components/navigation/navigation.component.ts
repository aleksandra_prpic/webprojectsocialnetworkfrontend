import { Component } from '@angular/core';
import {AuthService} from '../../services/auth-service/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
})
export class NavigationComponent  {
  constructor(private authService: AuthService, private router: Router, private toastr: ToastrService) {}
  isLoggedIn(){ return this.authService.isLoggedIn(); }
  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/');
    this.toastr.info('Logged out successfully.');
  }
}
