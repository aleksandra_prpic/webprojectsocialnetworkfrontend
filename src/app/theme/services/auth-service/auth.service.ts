import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';

import {User} from './user.model';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import { tokenNotExpired } from 'angular2-jwt';


@Injectable()
export class AuthService {
  private apiUrl = 'http://localhost:3002/auth/';
  private hostUrl = 'http://localhost:3002/';
  private headers = new Headers({'Content-Type': 'application/json'});
  constructor(private http: Http) {}

  login(user: User) {
    return this.http.post(`${this.apiUrl}login`, JSON.stringify(user), {headers: this.headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }

  register(user: User) {
    return this.http.post(`${this.apiUrl}register`, JSON.stringify(user), {headers: this.headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }

  changePassword(pwds: Object) {
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.post(`${this.hostUrl}users/${localStorage.getItem('userId')}/changepw${token}`,
      JSON.stringify(pwds), {headers: this.headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }

  logout() {
    localStorage.clear();
  }

  isLoggedIn() {
    return tokenNotExpired();
  }
}
