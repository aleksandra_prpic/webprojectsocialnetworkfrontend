export class User {
  constructor(public username: string,
              public password: string,
              public firstName?: string,
              public lastName?: string,
              public email?: string,
              public gender?: string,
              public mobileNumber?: string,
              public job?: string,
              public aboutMe?: string,
              public avatar?: string,
              public _id?: string
  ) {}
}
