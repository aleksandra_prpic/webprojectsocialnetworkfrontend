import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Project} from './project.model';


@Injectable()
export class ContentService {
  private apiUrl = 'http://localhost:3002/projects/';
  private hostUrl = 'http://localhost:3002/';
  private headers = new Headers({'Content-Type': 'application/json'});
  constructor(private http: Http) {}

  getAllProjects(comments = false, count = 5) {
    if (count === 0) {
      return this.http.get(`${this.apiUrl}?name=true&comments=${comments}`)
        .map((response: Response) => response.json())
        .catch((error: Response) => {
          return Observable.throw(error.json());
        });
    } else {
      return this.http.get(`${this.apiUrl}?name=true&comments=${comments}&count=${count}`)
        .map((response: Response) => response.json())
        .catch((error: Response) => {
          return Observable.throw(error.json());
        });
    }
  }

  getProjectsByUser(userId: string){
    return this.http.get(`${this.hostUrl}users/${userId}`)
      .map((response: Response) => response.json().projects)
      .catch((error: Response) => {
        return Observable.throw(error);
      });
  }

  getProjectById(projectId: string) {
    return this.http.get(`${this.apiUrl}${projectId}`)
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }

  addProject(project: Project) {
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.post(`${this.apiUrl}${token}`, JSON.stringify(project), {headers: this.headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }

  modifyProject(project: Project) {
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    project.link = (project.link === '') ? 'empty' : project.link;
    return this.http.put(`${this.apiUrl}${project._id}${token}`, JSON.stringify(project), {headers: this.headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }

  deleteProjectById(projectId: string) {
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.delete(`${this.apiUrl}${projectId}${token}`)
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }
  uploadFile(inputEl: HTMLInputElement, url: string, name: string) {
    let fileCount: number = inputEl.files.length;
    let formData = new FormData();
    if (fileCount > 0) {
      formData.append(name, inputEl.files.item(0));
      return this.http.put(`${url}?token=${localStorage.getItem('token')}`, formData)
        .map((res: Response) => res.json())
        .catch((error: Response) => {
          return Observable.throw(error.json());
        });
    }
  }

}
