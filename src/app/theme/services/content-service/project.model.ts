export class Project {
  constructor(public title: string,
              public content: string,
              public link?: string,
              public imageUrl?: string,
              public _id?: string,
              public user?: string,
              public comments?: string[],
              public createdTime?: Date
  ) {}
}
