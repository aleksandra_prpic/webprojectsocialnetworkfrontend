import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Comment} from './comment.model';


@Injectable()
export class CommentService {
  private apiUrl = 'http://localhost:3002/projects';
  private headers = new Headers({'Content-Type': 'application/json'});
  constructor(private http: Http) {}
  getComments(projectId: string) {
    return this.http.get(`${this.apiUrl}/${projectId}/comments`)
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }
  postComment(projectId: string, comment: Comment) {
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.post(`${this.apiUrl}/${projectId}/comments${token}`, JSON.stringify(comment), {headers: this.headers })
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }
  deleteComment(projectId: string, commentId: string){
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.delete(`${this.apiUrl}/${projectId}/${commentId}${token}`)
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }
}
