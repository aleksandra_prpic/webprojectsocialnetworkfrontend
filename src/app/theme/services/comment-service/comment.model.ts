export class Comment {
  constructor(public content: string,
              public _id?: string,
              public user?: string,
              public project?: string,
              public createdTime?: Date
  ) {}
}
