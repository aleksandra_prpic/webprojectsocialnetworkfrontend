import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../auth-service/user.model';

@Injectable()
export class UsersService {
  private apiUrl = 'http://localhost:3002/users/';
  private headers = new Headers({'Content-Type': 'application/json'});
  constructor(private http: Http) {}
  getAllUsers() {
    return this.http.get(`${this.apiUrl}`)
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }
  getUsers(count: number, random: boolean) {
    return this.http.get(`${this.apiUrl}?count=${count}&random=${random}`)
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }
  getUserById(userId: string){
    return this.http.get(`${this.apiUrl}${userId}`)
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }
  modifyUser(user: User, userId: string) {
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.put(`${this.apiUrl}${userId}${token}`, JSON.stringify(user), {headers: this.headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }
}
