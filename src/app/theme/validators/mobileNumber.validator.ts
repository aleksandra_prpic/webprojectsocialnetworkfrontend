import {AbstractControl} from '@angular/forms';

export class MobileNumberValidator {

  public static validate(c: AbstractControl) {
    let PHONE_REGEXP = /^[0-9]{1,10}$/;

    return PHONE_REGEXP.test(c.value) || c.value === '' ? null : {
      validateEmail: {
        valid: false
      }
    };
  }
}
