import {AbstractControl} from '@angular/forms';

export class GenderValidator {

  public static validate(c: AbstractControl) {


    return (c.value === 'male' || c.value === 'female' ) ? null : {
      validateEmail: {
        valid: false
      }
    };
  }
}
