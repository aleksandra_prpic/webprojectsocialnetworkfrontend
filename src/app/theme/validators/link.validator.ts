import {AbstractControl} from '@angular/forms';

export class LinkValidator {

  public static validate(c: AbstractControl) {
    let LINK_REGEXP = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;

    return LINK_REGEXP.test(c.value) || c.value === '' ? null : {
      validateLink: {
        valid: false
      }
    };
  }
}
